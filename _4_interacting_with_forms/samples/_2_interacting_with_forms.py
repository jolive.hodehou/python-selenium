import pytest
from time import sleep

from common import config, helpers
from selenium.webdriver import Remote


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver(driver_instance):
    driver_instance.get(config.WEB_SAMPLES_URL + "/sample3.html")
    yield driver_instance
    driver_instance.delete_all_cookies()


def test_1(driver: Remote):
    field1 = driver.find_element_by_id("field1")
    field1.clear()
    field1.send_keys("Value 1")

    field2 = driver.find_element_by_id("field2")
    field2.clear()
    field2.send_keys("Value 2")

    field3 = driver.find_element_by_id("field3")
    field3.clear()
    field3.send_keys("Value 3")

    checkboxes = driver.find_elements_by_class_name("md-checkbox-container")
    checkboxes[0].click()
    checkboxes[1].click()

    radios = driver.find_elements_by_class_name("md-radio-container")
    radios[1].click()

    switch1 = driver.find_element_by_class_name("md-switch-container")
    switch1.click()

    submit = driver.find_element_by_id("submit")
    submit.click()

    # Sleep for a moment to show the dialog
    sleep(1)
