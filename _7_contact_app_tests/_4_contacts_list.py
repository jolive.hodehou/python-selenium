import pytest
from selenium.webdriver import ActionChains
from selenium.webdriver import Remote
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


@pytest.fixture
def driver(login):
    # Returns already logged in user
    return login


def test_get_default_contacts_names(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_search_for_single_contact_by_name(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_filter_by_one_label(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_delete_selected_contacts(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_show_favorites(driver: Remote):
    # TODO Implement. Use WebDriverWait to wait for the snackbar
    pytest.fail("Not implemented! Remove this line.")


#
# Optional
#

def test_search_for_two_contacts_by_email(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_search_for_no_results(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_filter_by_two_labels(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")


def test_filter_by_non_existing_label(driver: Remote):
    # TODO Implement.
    pytest.fail("Not implemented! Remove this line.")
