from _2_pytest.samples.custom_list import CustomList


def test_clear():
    l = CustomList([1, 2, 3, 4, 5])
    l.clear()
    assert l.len() == 0


def test_len():
    l = CustomList([1, 2, 3, 4, 5])
    assert l.len() == 5
