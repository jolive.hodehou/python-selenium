import os
import random
import string

import pytest


@pytest.fixture
def temp_file():
    # random file name
    filename = ''.join(random.sample(string.ascii_lowercase, 10)) + ".tmp"
    # add some contents
    with open(filename, "w+") as file:
        # write to the file
        for i in range(1, 6):
            file.write(f"Line {i}\n")

    # return the file
    yield filename
    # remove the file
    os.remove(filename)


def test_read_tmp_file_contents(temp_file):
    with open(temp_file) as file:
        lines = file.read()
        assert "Line 1" in lines
        assert "Line 2" in lines
        assert "Line 3" in lines
        assert "Line 4" in lines
        assert "Line 5" in lines
