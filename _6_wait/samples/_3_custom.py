import pytest

from common import config, helpers
from selenium.webdriver import Remote
from selenium.webdriver.support.wait import WebDriverWait


@pytest.fixture(scope="module")
def driver_instance():
    driver = helpers.new_driver()
    yield driver
    driver.close()


@pytest.fixture
def driver():
    driver = helpers.new_driver()
    driver.get(config.WEB_SAMPLES_URL + "/sample6.html")
    yield driver
    driver.close()


def test_verifies_button_states(driver: Remote):
    wait = WebDriverWait(driver, 30)

    button = driver.find_element_by_xpath("//*[contains(text(), 'Load')]")
    button.click()

    assert wait.until(lambda _: driver.find_element_by_xpath("//*[contains(text(), 'Warming up')]"))
    assert wait.until(lambda _: driver.find_element_by_xpath("//*[contains(text(), 'Loading')]"))
    assert wait.until(lambda _: driver.find_element_by_css_selector(".md-progress-bar .md-progress-bar-fill").get_attribute("style") == "width: 100%;")
    assert wait.until(lambda _: driver.find_element_by_xpath("//*[contains(text(), 'Load')]").is_enabled())

    driver.save_screenshot(config.SCREENSHOTS_DIR + "/progress-bar.png")